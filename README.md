# README - Maad Decor Magento 2 Site#




### How to setup Repo on your server ###

* Clone directory
* Using the samples provided here: ____________ adjust and add env.php and config.php files to /app/etc
* Expand the contents provided here: ________________ and upload to the /pub directory
* Run composer install
* Clear cache from command line using `./mg cache:flush`

### Theme specific steps ###

***May not need to do the following, setting SHOULD be in the database***

* From command line, clear the cache using the following command `./mg cache:flush`
* From the command line, run the following commands, in order 
    * `./mg module:disable MGS_Mpanel --clear-static-content`
    * `./mg module:disable MGS_Mmegamenu --clear-static-content`
    * `./mg module:disable MGS_Portfolio --clear-static-content`
    * `./mg module:disable MGS_Testimonial --clear-static-content`
    * `./mg module:disable MGS_Brand --clear-static-content`
    * `./mg module:disable MGS_Core --clear-static-content`
    * `./mg module:disable MGS_Promobanners --clear-static-content`
    * `./mg module:disable MGS_StoreLocator --clear-static-content`
    * `./mg module:disable MGS_Blog --clear-static-content`
    * `./mg module:disable MGS_QuickView --clear-static-content`
* From the command line, recompile using this command `./mg setup:di:compile`